__author__ = 'east'
from model.user import User
from model.loginPage import LoginPage
from selenium_fixture import application
import time

def test_create_new_random_student(application):
    """
    Test checks if possible to create new student

    """
    application.go_to_home_page()
    application.login(User.admin())
    assert application.is_logged_in()
    application.go_to_student_page()
    application.go_to_create_student_page()
    application.create_new_random_student(User.random_rand())
    time.sleep(2)
    assert application.student_is_saved()
    assert application.student_is_found()