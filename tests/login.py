# -*- coding: utf-8 -*-
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.common.exceptions import *
from conftest import application
from model.user import User
# import pytest


def test_login_with_invalid_credentials(application):
    application.go_to_home_page()
    application.login(User.random())
    assert application.is_not_logged_in()


def test_login(application):
    application.go_to_home_page()
    application.login(User.admin())
    assert application.is_logged_in()
    application.logout()
    # assert application.is_not_logged_in()

