__author__ = 'east'
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.expected_conditions import *
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

wd = webdriver.Remote("dasda", webdriver.DesiredCapabilities.IPHONE.copy())
wd = webdriver.PhantomJS()
wd = webdriver.Ie()
wd = webdriver.Safari()
wd = webdriver.Chrome(desired_capabilities={})

wd.get("http://staging.maximumtest.pp.ua/")
# не относиться к AJAX
wd.refresh()
wd.back()
wd.forward()
element1 = wd.find_element_by_xpath("//a")
element2 = element1.find_element_by_xpath("//a2")
# ожидание
wd.implicitly_wait(10)

# best practice
wd.quit()
# закрывает и удаляет
# если грид - закрывает сессию

wd.close()
# закрывает последнее окно


def is_element_present(self, by , locator):
    return len(self.driver.find_elements(by, locator)) > 0

#Явное ожидание
wait = WebDriverWait(wd, 30)
wait = wait.until(lambda x:x.find_element(by, locator))

#Серия условий из експектед контидионт

element = wait.until(element_to_be_clickable())

dropdown = Select(element)

dropdown.select_by_index()
dropdown.select_by_value()
dropdown.select_by_visible_text()

# Добавить текст в конец
element1.click()
element1.send_keys(Keys.HOME)
element1.send_keys("some keys")

#Удалить текст
element1.send_keys(Keys.CONTROL, 'a')
time.sleep(1)
element1.send_keys(Keys.DELETE)

# Большой текст

set_clipboard_contents(longtext)
textarea.send_keys(Keys.CONTROL, 'v')

# Цепочки
webdriver.ActionChains(wd).move_to_element(el,1,1).click().perform()

# События Нативные и Синтезированные 1) На уровне ОС 2) Внутри браузера

driver = webdriver.Firefox(capabilities={'native_events': True})

# Actions
#не в центр элемента
webdriver.ActionChains(wd).move_to_element(el,1,1).click().perform()
#просто цепочка
webdriver.ActionChains(driver).move_to_element(drag).key_down(Keys.CONTROL).click_and_hold().move_to_element(drop).release().key_up(Keys.CONTROL).perform()
#относительно центра
webdriver.ActionChains(driver).move_to_element(element).move_by_offset(5, 5).click().perform()
#drag and drop
webdriver.ActionChains(driver).drag_and_drop(element, element1).perform()

#Всегда абсолютные ссылки ВСЕГДА (href, src)
element1.get_attribute("href")
button = driver.find_element_by_xpath("//")
button.get_attribute('disabled')
#либо null либо true

#Свойста
element1.value_of_css_property("color")
element1.size()
element1.location()
element1.get_tag_name()
element1.is_enabled()  # Для полей ввода
element1.is_displayed()
element1.is_selected()


#Свит ту
alert = driver.switch_to_alert()
alert_text = alert.text()
alert.accept()

# frames
# index or name accepts != Chrome
driver.switch_to_frame(driver.find_element_by_tag_name("iframe"))
# did something inside browser
driver.switch_to_default_content()

# Windows
all_windows = driver.window_handles()
this_window = driver.current_window_handle()

driver.switch_to_window(handle)
# do smth
driver.close()
driver.switch_to_window(original_window)

# Туда обратно смотри доп материалы


driver.get_window_position()
driver.get_window_size()
driver.maximize_window()


element = wait.until(presence_of_all_elements_located(By.LINK_TEXT, "Dashboard"))
