__author__ = 'east'


class User(object):

    def __init__(self, username=" ", secondname=" ", phonenum=" ", password=" ", email=" "):
        self.username = username
        self.secondname = secondname
        self.phonenum = phonenum
        self.password = password
        self.email = email

    @classmethod
    def admin(cls):
        return cls(username="admin", password='123qweasdzxc')

    @classmethod
    def random(cls):
        return cls(username="random", password="invalid")

    @classmethod
    def random_rand(cls):
        from random import randint
        return cls(username="user" + str(randint(0, 1000000)),
                   secondname="sec" + str(randint(0, 1000000)),
                   phonenum="9191919191",
                   email="user" + str(randint(0, 1000000)) + "@test.com",
                   password="123456")

    @classmethod
    def random_reg(cls):
        from random import randint
        return cls(username="userName",
                   secondname="secName",
                   phonenum="9191919191",
                   email="user" + str(randint(0, 1000000)) + "@test.com",
                   password="123456")