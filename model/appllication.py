__author__ = 'east'
# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.expected_conditions import *
#from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from pages.login_page import LoginPage
from pages.internal_page import InternalPage
from pages.createStudent import CreateStudent
from pages.adminstudent import AdminStudent
from pages.market.market import Market
from pages.market.authorization import AuthorizationPage
from pages.market.registration import RegistrationPage
from pages.market.checkout import CheckoutPage
from pages.student.todo import ToDoList
from pages.bandle_page import BundlePage
from pages.create_bundle_page import CreateBundlePage
from model.bundle import Bundle
from time import sleep
from selenium import webdriver

from pages.page import Page


class Appllication(object):

    def __init__(self, driver, base_url):

        self.driver = driver
        # driver.get(base_url)
        self.base_url = base_url
        self.login_page = LoginPage(driver, base_url)
        self.internal_page = InternalPage(driver, base_url)
        self.admin_student = AdminStudent(driver, base_url)
        self.create_student = CreateStudent(driver, base_url)
        self.authorization = AuthorizationPage(driver, base_url)
        self.registration = RegistrationPage(driver, base_url)
        self.checkout = CheckoutPage(driver, base_url)
        self.bundle_page = BundlePage(driver, base_url)
        self.create_bundle_page = CreateBundlePage(driver, base_url)
        self.todo = ToDoList(driver, base_url)
        self.market = Market(driver, base_url)
        self.wait = WebDriverWait(driver, 10)

    def go_to_home_page(self):
        driver = self.driver
        driver.get(self.base_url)

    def login(self, user):
        lp = self.login_page
        # assert lp.is_this_page
        lp.email_field.clear()
        lp.email_field.send_keys(user.username)
        lp.password_field.clear()
        lp.password_field.send_keys(user.password)
        lp.submit_button.click()

    def logout(self):
        lp = self.login_page
        lp.logout()

    def is_logged_in(self):
        admin = self.internal_page
        return admin.is_this_page

    def is_not_logged_in(self):
        lp = self.login_page
        return lp.failed_login_page

    def go_to_student_page(self):
        admin = self.internal_page
        driver = self.driver
        driver.get(self.base_url + "/admin/student")
        #driver.ActionChains(admin).people_menu_item.click().sleep(2).menu_item_students_in_people_menu.click().sleep().perform()
        # admin.people_menu_item.click()
        # sleep(5)
        # admin.menu_item_students_in_people_menu.click()

    def go_to_create_student_page(self):
        admin_student = self.admin_student
        admin_student.create_new_student_button.click()
        # add wait !!!

    def create_new_random_student(self, user):
        # add grab values from fields
        global first_name_for_search
        create_student = self.create_student
        # first name
        create_student.first_name_field.clear()
        first_name_for_search = user.username
        create_student.first_name_field.send_keys(first_name_for_search)
        # second name
        create_student.second_name_field.clear()
        create_student.second_name_field.send_keys(user.secondname)
        # email
        create_student.email_field.clear()
        create_student.email_field.send_keys(user.email)
        # phone
        create_student.phone_number_field.clear()
        create_student.phone_number_field.send_keys(user.phonenum)
        # password
        create_student.password_field.clear()
        create_student.password_field.send_keys(user.password)

        create_student.first_group_checkbox.click()
        # grab first name value for search
        first_name_for_search = create_student.first_name_field.get_attribute("value")
        create_student.save_button.click()

    def student_is_saved(self):
        admin = self.internal_page
        return admin.is_success_alert

    # rewrite to use email for search
    def student_is_found(self):
        global first_name_for_search
        student = self.admin_student
        student.name_field.send_keys(first_name_for_search)
        student.name_field.send_keys(Keys.RETURN)
        sleep(5)
        return student.is_student_found_by_name(first_name_for_search)

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! MARKET beneath !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    def go_to_market(self):
        self.driver.get(self.base_url + self.market.url)



    def is_basket_empty(self):
        market = self.market
        return market.basket_is_empty

    def add_free_course_to_basket(self):
        """
        just add free course to the basket on market page.
        if free course isn't present just crash (OOOOOPPPPSSS!!)

        It would be great to fix that method later such way that it works perfectly
        and creates or changes some course to free and add it after it =)

        But, maybe it's stupid idea too =)))))))

        """
        market = self.market
        market.find_free_course.click()

    def course_in_basket(self):
        """
        Maybe later I will need to check that user can add to the basket
        certain quantity of courses, so I gonna to fix this hard code that day =)

        """
        market = self.market
        return market.is_course_in_basket(1)

    def number_of_courses_in_basket(self):
        """
        fix later hard code

        """
        market = self.market
        print(market.is_number_of_courses_ok(1))
        return market.is_number_of_courses_ok(1)

    def is_present_basket_button(self):
        market = self.market
        return market.is_button_present

    def go_to_authorization_page(self):
        market = self.market
        market.buy_course_button.click()

    def go_to_registration_market_page(self):
        auth = self.authorization
        auth.link_to_registration_page.click()

    def sign_up_by_form_in_market(self, user):
        reg_mar = self.registration
        reg_mar.first_name_field.clear()
        reg_mar.first_name_field.send_keys(user.username)
        reg_mar.second_name_field.clear()
        reg_mar.second_name_field.send_keys(user.secondname)
        reg_mar.email_field.clear()
        reg_mar.email_field.send_keys(user.email)
        reg_mar.password_field.clear()
        password = user.password
        reg_mar.password_field.send_keys(password)
        reg_mar.confirm_password.clear()
        reg_mar.confirm_password.send_keys(password)
        reg_mar.phone_field.clear()
        reg_mar.phone_field.send_keys(user.phonenum)
        #self.driver = webdriver.Chrome()
        #self.driver.get_screenshot_as_file("/home/east/Workspace/Maximum-tests/screenshots/foo.png")
        self.wait.until(visibility_of(reg_mar.submit_button))

        reg_mar.submit_button.click()
        # self.driver.get_screenshot_as_file("/home/east/Workspace/Maximum-tests/screenshots/foo.png")

    def add_course(self):
        checkout = self.checkout
        checkout.agree_label.click()
        checkout.pay_button.click()

    def is_logged_in_at_checkout(self, user):
        ch = self.checkout
        return ch.is_logged_in_at_checkout(user.username, user.secondname)

    def is_free_course_in_ch_basket(self):
        ch = self.checkout
        return ch.is_button_for_free

    def is_correct_user_on_to_do(self, user):
        todo = self.todo
        return todo.is_user_correct(user.username, user.secondname)


# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! bundle !!!!!!!!!!!!!!!!!!!!!!!!!!!11

    def go_to_bundles_page(self):
        self.driver.get(self.base_url + self.bundle_page.url)

    def go_to_create_bundle_page(self):
        self.bundle_page.is_button_clickable.click()

    def create_random_bundle(self, bundle):
        global title_bundle
        create_bundle = self.create_bundle_page
        title_bundle = bundle.title
        create_bundle.bundle_name_field.send_keys(title_bundle)
        create_bundle.bundle_price_field.send_keys(bundle.price)
        for item in xrange(len(create_bundle.all_courses_check_box)):
            create_bundle.all_courses_check_box[item].click()
        create_bundle.is_save_button_clickable.click()

    def is_bundle_created(self):
        global title_bundle
        return self.bundle_page.is_bundle_present_by_title(title_bundle)