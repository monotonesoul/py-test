__author__ = 'east'


class Bundle:

    def __init__(self, title="", price="", quantity=""):
        self.title = title
        self.price = price
        self.quantity = quantity

    @classmethod
    def random_bundle(cls):
        from random import randint
        return cls(title="Bundle" + str(randint(0, 1000000)),
                   price= str(randint(0, 1000000)),
                   quantity=randint(1, 9))