__author__ = 'east'
from conftest import app
from model.user import User
from time import sleep


def test_user_can_add_free_course(app):
    """
    Very bad test =)
    """
    app.logout()
    app.go_to_market()
    # assert app.is_that_page()

    #assert app.is_basket_empty()
    app.add_free_course_to_basket()
    assert app.course_in_basket()
    assert app.number_of_courses_in_basket()
    assert app.is_present_basket_button()
    app.go_to_authorization_page()
    sleep(3)
    app.go_to_registration_market_page()
    # assert app.is_that_page()
    app.sign_up_by_form_in_market(User.random_reg())
    # assert app.is_logged_in_at_checkout(User.random_reg())
    # assert app.course_in_checkout_basket()
    # assert app.is_free_course_in_ch_basket()
    app.add_course()
    assert app.is_correct_user_on_to_do(User.random_reg())
    # assert app.is_course_added()
    #
    # app.go_to_market()
    # assert app.is_not_added_course_visible()