__author__ = 'east'
# -*- coding:utf-8 -*-
from pages.page import Page
from selenium.webdriver.common.by import By
from selenium import webdriver


class Market(Page):

    @property
    def url(self):
        return "market"

    @property
    def first_course_in_market(self):
        self.driver = webdriver.Chrome()
        add_button = self.driver.find_element_by_xpath(".//*[@id='main']/div[2]/div[1]/div/div[1]/div/div[2]/a")
        # Nahuya???
        return add_button if self.is_element_visible((By.XPATH, ".//*[@id='main']/div[2]/div[1]/div/div[1]/div/div[2]/a")) else False

    def are_courses_available(self):
        return self.driver.find_elements(By.XPATH, "//div[@class='course-schedule bg-orange']")

    @property
    def find_free_course(self):
        list_of_available = self.are_courses_available()
        free_course_block = None
        if list_of_available:
            for courses in list_of_available:
                if courses.find_elements(By.XPATH, "//span[contains(text() ,'бесплатно')]"):
                    free_course_block = courses
                    break
        return free_course_block.find_element_by_xpath(".//a[@class='btn buy bg-green ga-track']")

    @property
    def basket_is_empty(self):
        return self.is_element_visible((By.XPATH, "//p[@class='cart-is-empty-text']"))

    def is_course_in_basket(self, number):
        return self.is_element_visible((By.XPATH, "//ol[@class='cart-list']/li[%i]" % number))

    def is_number_of_courses_ok(self, number):
        return self.is_element_visible((By.XPATH, "//*[contains(@class, 'course-amount') and contains(. , %s) ]/span[1]" % number))

    @property
    def is_button_present(self):
        return self.is_element_visible((By.XPATH, "//*[contains(@href, '/market/checkout')]"))

    @property
    def buy_course_button(self):
        return self.driver.find_element_by_xpath("//*[contains(@href, '/market/checkout')]")
