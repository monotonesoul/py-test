__author__ = 'east'
from pages.page import Page
from selenium.webdriver.common.by import By
from selenium import webdriver


class RegistrationPage(Page):

    @property
    def first_name_field(self):

        return self.driver.find_element_by_xpath("//input[@id='first_name']")

    @property
    def second_name_field(self):
        return self.driver.find_element_by_xpath("//input[@id='last_name']")

    @property
    def email_field(self):
        return self.driver.find_element_by_xpath("//input[@id='email']")

    @property
    def password_field(self):
        return self.driver.find_element_by_xpath("//input[@id='password']")

    @property
    def confirm_password(self):
        return self.driver.find_element_by_xpath("//input[@id='confirmPassword']")

    @property
    def phone_field(self):
        return self.driver.find_element_by_xpath("//input[@id='phone']")

    @property
    def city_field(self):
        return self.driver.find_element_by_xpath("//*[@id='verticalForm']/fieldset/div[6]/span")

    @property
    def submit_button(self):
        return self.driver.find_element_by_xpath("//button")

    @property
    def facebook_btn(self):
        return self.driver.find_element_by_xpath(".//*[contains(@href, '/opauth/facebook?action=market_login')]")

    @property
    def vk_button(self):
        return self.driver.find_element_by_xpath(".//*[contains(@href, '/opauth/vkontakte?action=market_login')]")