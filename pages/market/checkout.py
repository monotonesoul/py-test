__author__ = 'east'
#-*- coding:utf-8 -*-
from  pages.page import Page
from selenium.webdriver.common.by import By
from selenium import webdriver


class CheckoutPage(Page):

    @property
    def agree_label(self):

        return self.driver.find_element_by_xpath("//div[@id='free-courses']/form/div/label")

    @property
    def pay_button(self):
        return self.driver.find_element_by_xpath(u"//input[@value='Начать обучение!']")

    def is_logged_in_at_checkout(self, username, secondname):
        criteria_of_search = str(username + ' ' + secondname)
        return self.is_element_visible((By.XPATH, "//div[contains(@class ,'user-profile')]/span[contains(text(), '%s')]" % criteria_of_search))

    @property
    def is_button_for_free(self):
        return self.is_element_visible((By.XPATH, u"//input[@value='Начать обучение!']"))