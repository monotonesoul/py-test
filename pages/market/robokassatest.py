__author__ = 'east'

from pages.page import Page


class RobokassaTest(Page):

    @property
    def go_to(self):
        return self.driver.find_element_by_xpath("[@id='ctl00_CPHMain_CToResultUrl_BGo']")

    @property
    def answer_text_area(self):
        return self.driver.find_element_by_xpath("//*[@id='ctl00_CPHMain_CToResultUrl_txtResultUrlResponse']")

    @property
    def success(self):
        return self.driver.find_element_by_xpath("//*[@id='ctl00_CPHMain_CToSuccessAndFailUrl_CReturnToShopSuccess_BReturn']")

    @property
    def fail(self):
        return  self.driver.find_element_by_xpath("//*[@id='ctl00_CPHMain_CToSuccessAndFailUrl_CReturnToShopFail_BReturn']")