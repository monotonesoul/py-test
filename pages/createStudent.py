__author__ = 'east'
# -*- coding: utf-8 -*-
from pages.page import Page
from selenium.webdriver.common.by import By


class CreateStudent(Page):

    @property
    def first_name_field(self):
        return self.driver.find_element_by_xpath("//*[@id='User_first_name']")

    @property
    def second_name_field(self):

        return self.driver.find_element_by_xpath("//*[@id='User_last_name']")

    @property
    def email_field(self):

        return self.driver.find_element_by_xpath("//*[@id='User_email']")

    @property
    def phone_number_field(self):

        return self.driver.find_element_by_xpath("//*[@id='User_phone']")

    @property
    def blocked_checkbox(self):
        return self.driver.find_element_by_xpath("//*[@id='User_blocked']")

    @property
    def password_field(self):
        return self.driver.find_element_by_xpath("//*[@id='UserAuth_password']")

    @property
    def first_group_checkbox(self):
        return self.driver.find_element_by_xpath("//*[@id='Student_educationGroupIDs_0']")

    @property
    def add_parent_button(self):
        return self.driver.find_element_by_xpath(".//a[@href='/admin/student/addParent/']")

    @property
    def save_button(self):
        return self.driver.find_element_by_xpath("//*[@id='yw0']")

    @property
    def is_that_page(self):
        return self.is_element_visible((By.XPATH, u"//h1[text()[contains(., 'Добавить нового студента')]]"))