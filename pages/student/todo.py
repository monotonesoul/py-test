__author__ = 'east'
# -*- coding:utf-8 -*-
from pages.page import Page
from selenium.webdriver.common.by import By
from selenium import webdriver


class ToDoList(Page):

    @property
    def is_user_correct(self, username, secondname):
        crit_for_search = str(username) + " " + str(secondname)
        return self.is_element_visible((By.XPATH, "//div[@class = 'student_name' and contains(text(), '%s')]" % crit_for_search))
