__author__ = 'east'
from pages.page import Page
from selenium import webdriver
from selenium.webdriver.common.by import By


class CreateBundlePage(Page):

    @property
    def url(self):
        # self.driver = webdriver.Chrome()
        return "/admin/bundleCourses/create"

    @property
    def bundle_name_field(self):
        return self.driver.find_element_by_xpath("id('BundleCourses_title')")

    @property
    def bundle_price_field(self):
        return self.driver.find_element_by_xpath("id('BundleCourses_cost')")

    @property
    def all_courses_check_box(self):
        return self.driver.find_elements_by_xpath(".//*[@name='yw3_c0[]']")

    @property
    def save_button(self):
        return self.driver.find_element_by_xpath("id('yw1')")

    @property
    def is_save_button_clickable(self):
        return self.is_element_visible_and_clickable((By.XPATH, "id('yw1')"))
