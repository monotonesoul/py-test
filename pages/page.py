__author__ = 'east'
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import *


class Page(object):
    def __init__(self, driver, base_url):
        self.driver = driver
        self.base_url = base_url
        self.wait = WebDriverWait(driver, 10)

    def is_element_visible(self, locator):
        try:
            return self.wait.until(visibility_of_element_located(locator))
        except WebDriverException:
            return False

    def logout(self):
        self.driver.get(self.base_url + "/signout")

    def is_that_page(self, url, locator):
        cur_url = self.driver.current_url()
        element = self.is_element_visible(locator)
        return True if cur_url and element else False

    def is_element_visible_and_clickable(self, locator):
        try:
            return self.wait.until(element_to_be_clickable(locator))
        except WebDriverException:
            return False