__author__ = 'east'
from pages.page import Page
from selenium.webdriver.common.by import By
from selenium import webdriver

class InternalPage(Page):

    @property
    def is_this_page(self):
        return self.is_element_visible((By.XPATH, ".//*[@id='content']/div[2]/h1"))

    @property
    def people_menu_item(self):

        return self.driver.find_element_by_xpath(".//*[@id='yw2']/li[3]/a")

    @property
    def menu_item_students_in_people_menu(self):
        return self.driver.find_element_by_xpath("//a[@href='/admin/student']")

    @property
    def is_success_alert(self):
        return self.is_element_visible((By.XPATH, "//div[@class='alert in alert-block fade alert-success']"))

