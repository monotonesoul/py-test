__author__ = 'east'
from pages.page import Page
from selenium import webdriver
from selenium.webdriver.common.by import By
class BundlePage(Page):

    @property
    def url(self):
        # self.driver = webdriver.Chrome()
        return "/admin/bundleCourses"

    @property
    def create_bundle_button(self):
        return self.driver.find_element_by_xpath(".//a[@href ='/admin/bundleCourses/create']")

    @property
    def is_button_clickable(self):
        return self.is_element_visible_and_clickable((By.XPATH, ".//a[@href ='/admin/bundleCourses/create']"))

    def is_bundle_present_by_title(self, title_bundle):
        return self.is_element_visible((By.XPATH, "//td[contains(text(),'%s')]" % title_bundle))