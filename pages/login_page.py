__author__ = 'east'
from pages.page import Page
from selenium.webdriver.common.by import By
from selenium import webdriver


class LoginPage(Page):

    @property
    def email_field(self):
        return self.driver.find_element_by_id("email")

    @property
    def password_field(self):
        return self.driver.find_element_by_id("password")

    @property
    def submit_button(self):
        return self.driver.find_element_by_id("login-btn")

    @property
    def vk_button(self):
        return self.driver.find_element_by_xpath("//*[@id='vk-login']")

    @property
    def facebook_button(self):
        return self.driver.find_element_by_xpath("//*[@id='fs-login']")

    @property
    def is_this_page(self):

        return self.is_element_visible((By.ID, 'login-btn'))

    @property
    def failed_login_page(self):
        return self.is_element_visible((By.XPATH, "//label[@for='current_user']"))