__author__ = 'east'
# -*- coding:utf-8 -*-
from pages.page import Page
from selenium.webdriver.common.by import By
from selenium import webdriver


class AdminStudent(Page):

    @property
    def create_new_student_button(self):
        # self.driver = webdriver.Chrome()
        return self.driver.find_element_by_xpath("//*[@href='/admin/student/create']")

    @property
    def name_field(self):
        return self.driver.find_element_by_xpath("//input[@id='Student_first_name']")

    def is_student_found_by_name(self, name):
        # fix this method
        return self.is_element_visible((By.XPATH, "//tr[1]/td[1][contains(text(), '%s')]" % name))