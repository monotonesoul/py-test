__author__ = 'east'
import pytest
from selenium import webdriver
from model.appllication import Appllication


def pytest_addoption(parser):
    parser.addoption("--browser", action="store", default="chrome", help="browser type")
    parser.addoption("--base_url", action="store", default="http://staging.maximumtest.pp.ua/", help="base URL")


@pytest.fixture(scope="session")
def browser_type(request):
    return request.config.getoption("--browser")


@pytest.fixture(scope="session")
def base_url(request):
    return request.config.getoption("--base_url")


@pytest.fixture(scope="session")
def app(request, browser_type, base_url):
    if browser_type == "firefox":
        driver = webdriver.Firefox()
    elif browser_type == "chrome":
        driver = webdriver.Chrome("/home/east/Downloads/chromedriver")
    elif browser_type == "phantomjs":
        driver = webdriver.PhantomJS()
    else:
        driver = webdriver.Ie()
    # driver.implicitly_wait(10)
    request.addfinalizer(driver.quit)
    return Appllication(driver, base_url)
