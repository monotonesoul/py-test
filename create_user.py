__author__ = 'east'
from model.user import User
from model.loginPage import LoginPage
from selenium_fixture import application
import time

def test_create_new_random_student(app):
    """
    Test checks if possible to create new student

    """
    app.go_to_home_page()
    app.login(User.admin())
    assert app.is_logged_in()
    app.go_to_student_page()
    app.go_to_create_student_page()
    app.create_new_random_student(User.random_rand())
    time.sleep(2)
    assert app.student_is_saved()
    assert app.student_is_found()