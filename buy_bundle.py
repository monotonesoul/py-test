from conftest import app
from model.user import User
from model.bundle import Bundle
from model.appllication import Appllication
from time import sleep


def test_admin_can_create_bundle(app):
    app.go_to_home_page()
    app.login(User.admin())
    app.go_to_bundles_page()
    app.go_to_create_bundle_page()
    app.create_random_bundle(Bundle.random_bundle())
    assert app.is_bundle_created()

# depended tests


def test_new_user_can_buy_bundle(app):
    app.go_to_bundles_page()
    app.grab_last_bundle_id()
    app.grab_quantity_of_courses()
    app.logout()
    app.go_to_checkout_by_bundle_link()
    app.app.go_to_registration_market_page()
    app.sign_up_by_form_in_market(User.random_reg())
    app.buy_course()
    assert app.is_bundle_added()




