# -*- coding: utf-8 -*-
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.common.exceptions import *
from conftest import app
from model.user import User
# import pytest


def test_login_with_invalid_credentials(app):
    app.go_to_home_page()
    app.login(User.random())
    assert app.is_not_logged_in()


def test_login(app):
    app.go_to_home_page()
    app.login(User.admin())
    assert app.is_logged_in()
    app.logout()
    # assert app.is_not_logged_in()

